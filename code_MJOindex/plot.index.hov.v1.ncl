; Plot a hovmoller of MIMIC data for looking at MJO events
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "png"
    fig_file = "~/Research/OBS/MIMIC/figs_MJOindex/index.hov.v1"
    
    verbose = False

    lat1 =  -15.
    lat2 =   25.
    lon1 =   50.
    lon2 =  180.
    
    t1 = (31+30)
    t2 = (31+30+31+30+31)-1

    threshold = 50.

;====================================================================================================
; Load data
;====================================================================================================
    idir   = "~/Data/OBS/MIMIC/"
    ifile  = systemfunc("ls -1 "+idir+"/*")
    infile = addfiles(ifile,"r")

    time   = tofloat( infile[:]->time )
    time := time(::8)
    time := time(t1:t2)
    time@units = "hours since 2014-08-01 00:00:00"


    TPW  = block_avg( infile[:]->mosaicTPW(:,{lat1:lat2},{lon1:lon2}) ,8)
    TPW := TPW(t1:t2,:,:)

if False then
    ;-----------------------------------------------------
    ; Create area index
    ;-----------------------------------------------------
    aindex = tofloat( dim_num_n(TPW.gt.threshold,(/1,2/)) )
    aindex = aindex - avg(aindex)
    aindex = aindex/stddev(aindex)  
    aindex!0    = "time"
    aindex&time = TPW&time
    ;-----------------------------------------------------
    ; Create plateau "intensity" index
    ;-----------------------------------------------------
    qindex = tofloat( dim_avg_n(where(TPW.gt.threshold,TPW,TPW@_FillValue),(/1,2/)) )
    qindex = qindex - avg(qindex)
    qindex = qindex/stddev(qindex)  
    qindex!0    = "time"
    qindex&time = TPW&time
    ;-----------------------------------------------------
    ; Calculate Amplitude and phase of new index
    ;-----------------------------------------------------
    AMP = sqrt( aindex^2. + qindex^2. )
    PHS = atan2(qindex,aindex)
end if
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(3,graphic)
        res = True
        res@gsnDraw             = False
        res@gsnFrame            = False
        lres = res
        res@cnLinesOn           = False
        res@cnFillOn            = True
        res@tmYLLabelFontHeightF= 0.005
        res@tmYLMinorOn         = False
        res@trYReverse          = True
        ;res@tmYLLabelAngleF     = 45.

        tmres = True
        tmres@ttmAxis           = "YL"
        tmres@ttmFormat         = "%C %D"
        ;tmres@ttmMajorStride    = 10
        tmres@ttmValues         = (/ (/2014, 8, 1,0,0,0/), \
                                     (/2014, 8,15,0,0,0/), \
                                     (/2014, 9, 1,0,0,0/), \
                                     (/2014, 9,15,0,0,0/), \
                                     (/2014,10, 1,0,0,0/), \
                                     (/2014,10,15,0,0,0/), \
                                     (/2014,11, 1,0,0,0/), \
                                     (/2014,11,15,0,0,0/), \
                                     (/2014,12, 1,0,0,0/), \
                                     (/2014,12,15,0,0,0/), \
                                     (/2015, 1, 1,0,0,0/)  /)
        tmres@ttmValues        := (/ (/2014, 8, 1,0,0,0/), \
                                     (/2014,11,20,0,0,0/), \
                                     (/2014,11,25,0,0,0/), \
                                     (/2014,11,30,0,0,0/), \
                                     (/2014,12, 5,0,0,0/), \
                                     (/2014,12,10,0,0,0/)  /)
        time_axis_labels(time,res,tmres)

    TPWavg  = dim_avg_n_Wrap(TPW,1)
    TPWarea = tofloat( dim_num_n(TPW.gt.threshold,(/1/)) )
    TPWval  = tofloat( dim_avg_n(where(TPW.gt.threshold,TPW,TPW@_FillValue),(/1/)) )

    copy_VarCoords(TPWavg,TPWarea)
    copy_VarCoords(TPWavg,TPWval)

    TPWarea@long_name = "TPW Area"
    TPWval@long_name  = "TPW Value"

    plot(0) = gsn_csm_contour(wks,TPWavg,res)
    plot(1) = gsn_csm_contour(wks,TPWarea,res)
    plot(2) = gsn_csm_contour(wks,TPWval,res)

        lres@xyLineThicknessF = 4.
    do p = 0,dimsizes(plot)-1
        xx = (/-1e3,1e3/)
        yy = (/1.,1./)
        overlay(plot(p),gsn_csm_xy(wks,xx,yy*time(31+20-1),lres))
        overlay(plot(p),gsn_csm_xy(wks,xx,yy*time(31+30-1),lres))
        overlay(plot(p),gsn_csm_xy(wks,xx,yy*time(31+30+10-1),lres))
        overlay(plot(p),gsn_csm_xy(wks,yy*90,xx*max(time),lres))
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = setres_panel() 

    gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)

    trimPNG(fig_file)
  
;====================================================================================================
;====================================================================================================
end