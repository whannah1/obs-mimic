#!/usr/bin/env python
#=========================================================================================
# 	This script converts MIMIC data from the tropic.ssec.wisc.edu server to NetCDF files
#	Uses a Java script to do the conversion (AreaToNetCDF.jar)
#	v1 -> ?
#
#	Oct., 2014	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy
import scipy
import Nio
#=========================================================================================
#=========================================================================================

cmd = "java -cp ~/AreaToNetCDF.jar AreaToNetCDF "

SRC_DIR 	= "/data2/whannah/OBS/MIMIC/raw_wisc_area/"
DST_DIR 	= "/data2/whannah/OBS/MIMIC/raw_wisc_nc/"


yr = "2008"
m1 = 1
m2 = 12
#=========================================================================================
#=========================================================================================
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

for m in range(m1,m2+1):
	mn = "{:0>2d}".format(m)
	for d in range(1,days_per_month[m-1]+1):
		dy = "{:0>2d}".format(d)
		for h in range(0,24):
			hr = "{:0>2d}".format(h)
			fname = yr+mn+dy+"T"+hr+"0000"
			ifile = SRC_DIR+fname+".area"
			ofile = DST_DIR+fname+".nc"
			os.system(cmd+" "+ifile+" "+ofile)

#=========================================================================================
#=========================================================================================
