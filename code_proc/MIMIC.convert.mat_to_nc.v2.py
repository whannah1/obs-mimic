#!/usr/bin/env python
#=========================================================================================
# 	This script converts MIMIC hourly *.mat files from idir 
#	v1 -> into daily NetCDF files that contain 24 hourly samples
#	v2 -> into monthly NetCDF files that contain 6 hourly samples
#	output files are written to odir
#
#	Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#	Also, check that 'var_name' contains the variables you want to convert
#
#	SciPy.io has a 'loadmat' function that is used for reading '.mat' files
#
#	The PyNIO (Nio) package is used for writing NetCDF files
#	and can be obtained from https://www.earthsystemgrid.org/home.htm
#
#    July, 2014	Walter Hannah 		University of Miami
#=========================================================================================
import os
import numpy as np
import numpy.ma as ma
import scipy as sp
import scipy.io as spio
import Nio
#=========================================================================================
#=========================================================================================
debug    = False 	# writes to 'testfile' instead of to odir
verbose	 = False	# verbose output for debugging

#testfile = '/Users/whannah/Research/OBS/MIMIC/test.nc'	# only for debug mode
testfile = '/home/whannah/Research/OBS/MIMIC/test.nc'	# only for debug mode
	
#idir = "/Users/whannah/Research/OBS/MIMIC/raw_mat/"		# input directory
#odir = "/Users/whannah/Research/OBS/MIMIC/data/"		# output directory
idir = "/data2/whannah/OBS/MIMIC/raw_mat/"			# input directory
odir = "/data2/whannah/OBS/MIMIC/data/"				# output directory

#define variables that will go into output NetCDF file
#var_name = ["mosaicTPW","mosaicTPWbefore","mosaicTPWafter","tdBeforeMx","tdAfterMx"]
var_name = ["mosaicTPW","TPW_TEND"]
#var_name = ["TPW_TEND"]

yr = "2014"
m1 = 5
m2 = 12
#=========================================================================================
#=========================================================================================
#mval = -999.
mval = 9.969209968386869e+36
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]
print
for m in range(m1,m2+1):
	ntime = days_per_month[m-1]*4
	print "m: ",m,"   days: ",days_per_month[m-1]
	if debug : ntime = 8
	#---------------------------------------------------
	# Create new NetCDF file for output
	#---------------------------------------------------
	mn = "{:0>2d}".format(m)
	ofile = odir+"MIMIC.with_AT.v2."+yr+mn+".nc"
	if debug: ofile  = testfile
	if os.path.isfile(ofile): os.remove(ofile)
	#outfile 		= Nio.open_file(ofile, "c",format='Classic')
	outfile 		= Nio.open_file(ofile, "c")
	outfile.history = ""
	print ""+ofile
	#---------------------------------------------------
	# Create coordinate variables and write to NetCDF
	#---------------------------------------------------
	matfile = spio.loadmat(idir+yr+mn+"01T010000vars.mat")

	#print(matfile)
	#tmp = matfile['tdBeforeMx'].astype(np.float32, copy=False)
	#tmp = np.where( ~np.isfinite(tmp), 0.0, tmp )
	#print np.min(tmp),"    ",np.max(tmp)
	#tmp = matfile['tdAfterMx'].astype(np.float32, copy=False)
	#tmp = np.where( ~np.isfinite(tmp), 0.0, tmp )
	#print np.min(tmp),"    ",np.max(tmp)
	#exit()
				
	dims = matfile["mosaicTPW"].shape
	nlat = int(dims[0])
	nlon = int(dims[1]) - 1
	time = sp.arange(ntime) *6
	lat  = sp.linspace( matfile["minLat"][0,0], matfile["maxLat"][0,0], nlat )
	tlon = sp.linspace( matfile["minLon"][0,0], matfile["maxLon"][0,0], nlon+1 )
	lon  = tlon[:nlon]
	outfile.create_dimension('time', ntime)
	outfile.create_dimension('lat' , nlat)
	outfile.create_dimension('lon' , nlon)	
	#---------------------------------------------------
	# TIME
	if verbose: print "  Writing file coordinate: time ("+str(ntime)+")"
	time 		= time.astype(np.int32, copy=False)		
	ftime 		= outfile.create_variable('time', 'i', ('time',))
	ftime[:]	= time[:ntime]
	ftime.units = 'hours since '+yr+'-'+mn+'-01 00:00:00'
	del time
	#---------------------------------------------------
	# LATITUDE
	if verbose: print "  Writing file coordinate: lat ("+str(nlat)+")"
	flat 		= outfile.create_variable('lat', 'd', ('lat',))
	flat[:]	 	= lat
	flat.units 	= 'degrees_north'
	#---------------------------------------------------
	#LONGITUDE
	if verbose: print "  Writing file coordinate: lon ("+str(nlon)+")"
	flon 		= outfile.create_variable('lon', 'd', ('lon',))
	flon[:]	 	= lon
	flon.units 	= 'degrees_east'
	#---------------------------------------------------
	#---------------------------------------------------
	for var in var_name:
		if var=="mosaicTPW" 		: var_info = ["mm","Total Precipitable Water"]
		if var=="mosaicTPWbefore" 	: var_info = ["mm","Total Precipitable Water"]
		if var=="mosaicTPWafter" 	: var_info = ["mm","Total Precipitable Water"]
		if var=="tdBeforeMx" 		: var_info = ["s","td After Mx"]
		if var=="tdAfterMx" 		: var_info = ["s","td Before Mx"]
		if var=="TPW_TEND"	 		: var_info = ["mm/s","TPW Tendency"]
		ftmp = outfile.create_variable(var, 'd', ('time','lat','lon'))
		ftmp.units 			= var_info[0]
		ftmp.long_name		= var_info[1]
		ftmp._FillValue 	= mval
		ftmp.missing_value 	= mval
		
		for d in range(1,days_per_month[m-1]+1):
		#for d in range(1,ntime/4+1):
			for h in range(0,24):
			#for h in range(18,23):
				#if m==7 and d==5 and h==22: continue				# not sure what's wrong with this file!
				first = (h==0 and d==0 and var==var_name[0])
				dy = "{:0>2d}".format(d)
				hr = "{:0>2d}".format(h)
				ifile = idir+yr+mn+dy+"T"+hr+"0000vars.mat"
				#ifile = "/data2/whannah/OBS/MIMIC/raw_mat/20140705T220000vars.mat"
				if h%6==0: print "    "+ifile
				matfile = spio.loadmat(ifile, verify_compressed_data_integrity=False)
				#---------------------------------------------------
				# print verbose information if verbose==True
				#---------------------------------------------------
				if verbose and first:
					print
					print matfile['__header__']
					print matfile['__globals__']
					print matfile['__version__']
					print("Input matlab file variables:")
					for key in matfile.keys():
						if type(matfile[key]).__name__ == 'ndarray':
						   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__,"  ",matfile[key].shape
						else:
						   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__
					print
				#---------------------------------------------------
				# Load data from mat files
				#---------------------------------------------------
				if first and verbose: print "  Writing file data: "+var+" ",tmp.shape
				if var=="TPW_TEND":
					iTPW1 = matfile['mosaicTPWbefore'][:,:nlon]#.astype(np.float32, copy=False)
					iTPW2 = matfile['mosaicTPWafter'][:,:nlon]#.astype(np.float32, copy=False)
					itd1  = matfile['tdBeforeMx'][:,:nlon]#.astype(np.float32, copy=False)
					itd2  = matfile['tdAfterMx'][:,:nlon]#.astype(np.float32, copy=False)
					itmp = ( iTPW2 - iTPW1 ) / ( itd2 + itd1 )
					del iTPW1,iTPW2,itd1,itd2
				else: 
					itmp = matfile[var][:,:nlon].astype(np.float32, copy=False)
				
				#---------------------------------------------------
				# Handle missing values (NaN's and large numbers)
				#---------------------------------------------------
				#itmp = np.where( np.isnan(itmp), mval, itmp )
				itmp = np.where( ~np.isfinite(itmp), mval, itmp )
				tmp = ma.masked_where( itmp>1e10, itmp )
				tmp.fill_value = mval
				
				#---------------------------------------------------
				# write data to NetCDF file
				#---------------------------------------------------
				if h%6==0 : otmp = tmp
				if h%6!=0 : 
					c1 = float(h%6)-1.
					c2 = float(h%6)
					otmp = ( tmp +  c1*otmp  )/c2 
				if h%6==5 : 
					ftmp[(d-1)*4+h/6,:,:] = otmp
					del itmp,tmp,otmp
				
	#---------------------------------------------------
	# Close the NetCDF file
	#---------------------------------------------------
	outfile.close()
	print
	if debug: exit()
	


#=========================================================================================
#=========================================================================================
