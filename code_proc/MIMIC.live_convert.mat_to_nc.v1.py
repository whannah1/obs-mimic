#!/usr/bin/env python
#=========================================================================================
# 	This script converts MIMIC hourly *.mat files from idir 
#	into daily NetCDF files that contain 24 hourly samples
#	output files are written to odir
#
#	Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#	Also, check that 'var_name' contains the variables you want to convert
#
#	SciPy.io has a 'loadmat' function that is used for reading '.mat' files
#
#	The PyNIO (nio) package is used for writing NetCDF files
#	and can be obtained from https://www.earthsystemgrid.org/home.htm
#
#    July, 2013	Walter Hannah 		University of Miami
#=========================================================================================
import os
from datetime import date
import numpy as np
import scipy as sp
import scipy.io as spio
import nio
#=========================================================================================
#=========================================================================================
debug    = True 	# writes to 'testfile' instead of to odir
verbose	 = True		# verbose terminal output for monitoring progress

testfile = '/home/whannah/Research/OBS/MIMIC/test.nc'		# only used for debug = True
	
#idir = "/Users/whannah/Research/OBS/MIMIC/raw_mat/"		# input directory
#odir = "/Volumes/Amaterasu/obs/MIMIC/data/"				# output directory
idir = "/home/whannah/Research/OBS/MIMIC/raw_mat/"			# input directory
odir = "/home/whannah/Research/OBS/MIMIC/data/"				# output directory

#define variables that will go into output NetCDF file
var_name = ["mosaicTPW","mosaicTPWbefore","mosaicTPWafter","tdBeforeMx","tdAfterMx"]

yr = date.today().year
m1 = 7
m2 = 7

ntime = 10

#=========================================================================================
# specify coordiante arrays
#=========================================================================================
nlat = 481
nlon = 1441
if debug:
	nlat = 5
	nlon = 5
lat  = sp.linspace(  -60., 60., nlat )
lon  = sp.linspace( -180.,180., nlon )
#=========================================================================================
# find which files are available in idir
#=========================================================================================
ifiles = os.listdir(idir)
nfiles = len(ifiles)
iyr = np.zeros(nfiles, dtype=np.int)
imn = np.zeros(nfiles, dtype=np.int)
idy = np.zeros(nfiles, dtype=np.int)
ihr = np.zeros(nfiles, dtype=np.int)

for f in xrange(0,nfiles):
	iyr[f] = ifiles[f][:4]
	imn[f] = ifiles[f][4:6]
	idy[f] = ifiles[f][6:8]
	ihr[f] = ifiles[f][9:11]
f = 0
print "first file : ",iyr[f],imn[f],idy[f],ihr[f]
f = nfiles-1
print "last file  : ",iyr[f],imn[f],idy[f],ihr[f]
#=========================================================================================
# find what data is missing in <odir>/<ofile>.nc
#=========================================================================================
if debug:
	ofile = testfile
	if os.path.isfile(ofile): os.remove(testfile)
else:
	ofile  = odir+"MIMIC."+str(yr)+".nc"
	
if os.path.isfile(ofile): 
	outfile = nio.open_file(ofile, "w")
else:
	outfile = nio.open_file(ofile, "c")
	outfile.create_dimension('time',None)
	outfile.create_dimension('lat' , nlat)
	outfile.create_dimension('lon' , nlon)	
	#---------------------------------------------
	# LATITUDE
	#---------------------------------------------
	if verbose: print "  Creating file coordinate: lat ("+str(nlat)+")"
	outfile.create_variable('lat', 'd', ('lat',)) 
	setattr(outfile.variables['lat'], 'standard_name', 'latitude') 
	setattr(outfile.variables['lat'], 'units', 'degrees_north') 
	setattr(outfile.variables['lat'], 'axis', 'Y') 
	outfile.variables['lat'].assign_value(lon) 
	#---------------------------------------------
	# LONGITUDE 
	#---------------------------------------------
	if verbose: print "  Creating file coordinate: lon ("+str(nlon)+")"
	outfile.create_variable('lon', 'd', ('lon',)) 
	setattr(outfile.variables['lon'], 'standard_name', 'longitude') 
	setattr(outfile.variables['lon'], 'units', 'degrees_east') 
	setattr(outfile.variables['lon'], 'axis', 'X') 
	outfile.variables['lon'].assign_value(lon) 
	#---------------------------------------------
	# TIME
	#---------------------------------------------
	if verbose: print "  Creating file coordinate: time (unlimited)"
	ftime = outfile.create_variable('time', 'd', ('time',)) 
	setattr(outfile.variables['time'], 'standard_name', 'Valid Time') 
	setattr(outfile.variables['time'], 'units', 'hours since '+str(yr)+'-01-01 00:00:00') 
	setattr(outfile.variables['time'], 'time_origin', ''+str(yr)+'-01-01 00:00:00') 
	setattr(outfile.variables['time'], 'calendar', 'gregorian') 
	setattr(outfile.variables['time'], 'axis', 'T')
	#---------------------------------------------
	# TPW
	#---------------------------------------------
	if verbose: print "  Creating file variable: TPW (time,lat,lon)"
	ftime = outfile.create_variable('TPW', 'd', ('time','lat','lon')) 
	setattr(outfile.variables['TPW'], 'standard_name', 'Total Precipitable Water') 
	setattr(outfile.variables['TPW'], 'long_name', 'Total Precipitable Water') 
	setattr(outfile.variables['TPW'], 'units', 'mm') 
	#---------------------------------------------
	#---------------------------------------------
	if True :
		tmp = np.ones((nlat,nlon))
		#---------------------------------------------
		# Write some fake data
		#---------------------------------------------
		if verbose: print "  Writing file variable data: TPW "
		fTPW = outfile.variables['TPW']
		for t in xrange(0,10):
			print "    t: ",t
			fTPW[t,:,:]	= t #* tmp
			ftime[t] 	= t * 24.
	#---------------------------------------------
	#---------------------------------------------

#=========================================================================================
# find what data is missing in <odir>/<ofile>.nc
#=========================================================================================
print "----------------------------------------------------------------------------------"
print "----------------------------------------------------------------------------------"
print outfile
print "----------------------------------------------------------------------------------"
print "----------------------------------------------------------------------------------"

ftime = outfile.variables['time']
fTPW  = outfile.variables['TPW']
ntime = len(ftime)
print "ntime",ntime
for t in xrange(0,ntime):
	print fTPW[t,0,0]
print "!"
	
exit()
#=========================================================================================
#=========================================================================================
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]
print
for m in range(m1,m2+1):
	for d in range(1,days_per_month[m-1]+1):
		mn = "{:0>2d}".format(m)
		dy = "{:0>2d}".format(d)
		#---------------------------------------------------
		# Create new NetCDF file for output
		#---------------------------------------------------
		ofile = odir+"MIMIC.with_AT."+yr+mn+dy+".nc"
		if debug: ofile  = testfile
		if os.path.isfile(ofile): os.remove(ofile)
		outfile 		= nio.open_file(ofile, "c")
		outfile.history = ""
		print ""+ofile
		#---------------------------------------------------
		# Loop through variables
		#---------------------------------------------------
		for var in var_name:
			ntime = 24
			for h in range(0,ntime):
				hr = "{:0>2d}".format(h)
				ifile = idir+yr+mn+dy+"T"+hr+"0000vars.mat"
				matfile = spio.loadmat(ifile)
				#---------------------------------------------------
				# print verbose information if verbose==True
				#---------------------------------------------------
				if verbose and h==0:
					print
					print matfile['__header__']
					print matfile['__globals__']
					print matfile['__version__']
					print("Input matlab file variables:")
					for key in matfile.keys():
						if type(matfile[key]).__name__ == 'ndarray':
						   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__,"  ",matfile[key].shape
						else:
						   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__
					print
				#---------------------------------------------------
				# Create coordinate variables
				#---------------------------------------------------
				dims = matfile["mosaicTPW"].shape
				
				nlat = int(dims[0])
				nlon = int(dims[1])
				lat  = sp.linspace( matfile["minLat"][0,0], matfile["maxLat"][0,0], nlat )
				lon  = sp.linspace( matfile["minLon"][0,0], matfile["maxLon"][0,0], nlon )
				#---------------------------------------------------
				# Write coordinate variables to output NetCDF file
				#---------------------------------------------------
				if h==0 and var==var_name[0]:
					if verbose: print "  Writing file coordinate: time ("+str(ntime)+")"
					outfile.create_dimension('time', ntime)
					time = sp.arange(ntime)
					time = time.astype(np.int32, copy=False)		
					ftime 		= outfile.create_variable('time', 'i', ('time',))
					ftime[:]	= time
					ftime.units = 'hours since '+yr+'-'+mn+'-'+dy+' 00:00:00'
					del time

					if verbose: print "  Writing file coordinate: lat ("+str(nlat)+")"
					outfile.create_dimension('lat' , nlat)
					flat 		= outfile.create_variable('lat', 'd', ('lat',))
					flat[:]	 	= lat
					flat.units 	= 'Degrees North'

					if verbose: print "  Writing file coordinate: lon ("+str(nlon)+")"
					outfile.create_dimension('lon' , nlon)	
					flon 		= outfile.create_variable('lon', 'd', ('lon',))
					flon[:]	 	= lon
					flon.units 	= 'Degrees East'
					print
				#---------------------------------------------------
				# Transfer data from '.mat' files to output file
				#---------------------------------------------------
				tmp = matfile[var]
				tmp = tmp.astype(np.float32, copy=False)
				if h == 0: 
					if verbose: print "  Writing file data: "+var+" ",tmp.shape
					ftmp = outfile.create_variable(var, 'f', ('time','lat','lon'))
					ftmp.units 		= 'mm'
					ftmp.long_name	= "Total Precipitable Water"
				ftmp[h,:,:]			= tmp
				del tmp
		#---------------------------------------------------
		# Close the NetCDF file
		#---------------------------------------------------
		outfile.close()
		print
		if debug: exit()
#=========================================================================================
#=========================================================================================
