#!/usr/bin/env python
#=========================================================================================
# 	This script transfers MIMIC data from the weather.rsmas.miami.edu server (i.e. SRC_DIR)
#	to a local directory specified by DST_DIR
#	Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#
#	After running this script, data can be converted from '.mat' to NetCDF using:
#	convert.mat_to_nc.py
#
#	July, 2013	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
#import numpy as np
#import scipy.io
#=========================================================================================
#=========================================================================================
server 		= ""
#cmd 		= "rsync -uavt "
cmd 		= "wget"
file_type 	= ".mat"

SRC_DIR 	= "http://weather.rsmas.miami.edu/repository/entry/get/"+\
              "RSMAS-UM+Repository+for+atm-ocean+data+and+its+science/"+\
              "Datasets+hosted+on+this+UM+server+/MIMIC+matlabs/"
             

SRC_DIR 	= "http://weather.rsmas.miami.edu/repository/entry/get/"+\
              "RSMAS-UM%20Repository%20for%20atm-ocean%20data%20and%20its%20science/"+\
              "Datasets%20hosted%20on%20this%20UM%20server%20/MIMIC%20and%20TPW%20budget/"+\
              "MIMIC%20matlabs/"

print SRC_DIR
exit()

DST_DIR 	= "~/Data/OBS/MIMIC/raw_mat/"

yr = "2014"
m1 = 1
m2 = 12
#=========================================================================================
#=========================================================================================
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

for m in range(m1,m2+1):
	for d in range(1,days_per_month[m-1]+1):
		for h in range(0,24):
			mn = "{:0>2d}".format(m)
			dy = "{:0>2d}".format(d)
			hr = "{:0>2d}".format(h)
			file_name = yr+mn+dy+"T"+hr+"0000vars"
			print file_name
			os.system(cmd+" "+SRC_DIR+"/"+file_name+file_type+" -P "+DST_DIR)
#=========================================================================================
#=========================================================================================
