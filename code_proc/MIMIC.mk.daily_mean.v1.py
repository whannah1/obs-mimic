#!/usr/bin/env python
#=========================================================================================
# 	This script converts MIMIC data from the tropic.ssec.wisc.edu server to NetCDF files
#	Uses a Java script to do the conversion (AreaToNetCDF.jar)
#	v1 -> ?
#
#	Oct., 2014	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy
import scipy
import Nio
#=========================================================================================
#=========================================================================================


SRC_DIR 	= "/data2/whannah/OBS/MIMIC/raw_wisc_nc/"
DST_DIR 	= "/data2/whannah/OBS/MIMIC/daily_data/"


yr = "2008"
m1 = 1
m2 = 1
#=========================================================================================
#=========================================================================================
mval = 9.969209968386869e+36
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

ofile = DST_DIR+"MIMIC.daily."+yr+".nc"
if os.path.isfile(ofile): os.remove(ofile)
outfile = Nio.open_file(ofile, "c")

dims = [365,480,1440]
ntime = int(dims[0])
nlat  = int(dims[1])
nlon  = int(dims[2]) - 1
time  = scipy.arange(ntime) *6
lat   = scipy.linspace( -60., 60., nlat )
lon   = scipy.linspace( -180, 180-0.25, nlon )

outfile.create_dimension('time', ntime)
outfile.create_dimension('lat' , nlat)
outfile.create_dimension('lon' , nlon)

ftmp = outfile.create_variable("TPW", 'd', ('time','lat','lon'))
ftmp.units 			= "mm"
ftmp.long_name		= "MIMIC Total Precipitable Water"
ftmp._FillValue 	= mval
ftmp.missing_value 	= mval

for m in range(m1,m2+1):
	mn = "{:0>2d}".format(m)
	for d in range(1,days_per_month[m-1]+1):
		dy = "{:0>2d}".format(d)
		for h in range(0,24):
			hr = "{:0>2d}".format(h)
			fname = yr+mn+dy+"T"+hr+"0000"
			ifile = SRC_DIR+fname+".nc"
			infile = Nio.open_file(ifile, "r")
			itmp = infile.variables["image"][0,:,:]
			if h == 0 : tmp = itmp
			if h != 0 : tmp = (tmp*h+itmp)/(h+1)
			del itmp
		print tmp.shape
		print ftmp.shape
		ftmp[d,:,:] = tmp
		print ofile
		exit()

outfile.close()			
			
#=========================================================================================
#=========================================================================================
