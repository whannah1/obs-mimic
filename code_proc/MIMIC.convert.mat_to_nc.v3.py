#!/usr/bin/env python
#=========================================================================================
#   This script converts MIMIC hourly *.mat files from idir 
#   v1 -> into daily NetCDF files that contain 24 hourly samples
#   v2 -> into monthly NetCDF files that contain 6 hourly samples
#   v3 -> into monthly NetCDF files that contain 3 hourly samples (based on v2)
#   output files are written to odir. 
#
#    Data is NOT averaged. Just resampled 3-hourly. (i.e. hour 0 and 3 are used, but hours 1,2 and 4,5 are not)
#
#    Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#    Also, check that 'var_name' contains the variables you want to convert
#
#    SciPy.io has a 'loadmat' function that is used for reading '.mat' files
#
#    The PyNIO (Nio) package is used for writing NetCDF files
#    and can be obtained from https://www.earthsystemgrid.org/home.htm
#
#    There was a reason why I could not use scipy to write out the netcdf files
#    but I forget what that was... PyNIO works well though
#
#    July, 2014 Walter Hannah       University of Miami
#=========================================================================================
import os
import numpy as np
import numpy.ma as ma
import scipy as sp
import scipy.io as spio
import Nio
import subprocess
home = os.getenv("HOME")
#=========================================================================================
#=========================================================================================
debug    = False     # writes to 'testfile' instead of to odir
verbose  = True    # verbose output for debugging

testfile = home+'/Data/OBS/MIMIC/test.nc'   # only for debug mode
    
idir = home+"/Data/OBS/MIMIC/raw_mat/"          # input directory
odir = home+"/Data/OBS/MIMIC/data/"             # output directory

#idir = str( os.system("ls -1d "+idir) )
#odir = str( os.system("ls -1d "+odir) )

idir =  os.popen("ls -1d "+idir).readlines()[0][:-1]
odir =  os.popen("ls -1d "+odir).readlines()[0][:-1]

print odir[:-1]
#exit()

#define variables that will go into output NetCDF file
#var_name = ["TPW_TEND","mosaicTPW","mosaicTPWbefore","mosaicTPWafter","tdBeforeMx","tdAfterMx"]
var_name = ["TPW_TEND","mosaicTPW"]



yr = "2015"
m1 = 1
m2 = 12

if debug : 
    m1 = 7
    m2 = m1

nh = 3          # hourly timestep
nt = 24/nh      # num timesteps per day
if debug : nt = nh
#=========================================================================================
#=========================================================================================
#mval = -999.
mval = 9.969209968386869e+36
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]
print
for m in range(m1,m2+1):
    ntime = days_per_month[m-1] * nt
    print "m: ",m,"   days: ",days_per_month[m-1]
    if debug : ntime = 8
    #---------------------------------------------------
    # Create new NetCDF file for output
    #---------------------------------------------------
    mn = "{:0>2d}".format(m)
    ofile = odir+"MIMIC.with_AT.v3."+yr+mn+".nc"
    if debug: ofile  = testfile
    if os.path.isfile(ofile): os.remove(ofile)
    #outfile        = Nio.open_file(ofile, "c",format='Classic')
    outfile         = Nio.open_file(ofile, "c")
    outfile.history = ""
    print ""+ofile
    #---------------------------------------------------
    # Create coordinate variables and write to NetCDF
    #---------------------------------------------------
    matfile = spio.loadmat(idir+yr+mn+"01T010000vars.mat")

    #print(matfile)
    #tmp = matfile['tdBeforeMx'].astype(np.float32, copy=False)
    #tmp = np.where( ~np.isfinite(tmp), 0.0, tmp )
    #print np.min(tmp),"    ",np.max(tmp)
    #tmp = matfile['tdAfterMx'].astype(np.float32, copy=False)
    #tmp = np.where( ~np.isfinite(tmp), 0.0, tmp )
    #print np.min(tmp),"    ",np.max(tmp)
    #exit()
                
    dims = matfile["mosaicTPW"].shape
    nlat = int(dims[0])
    nlon = int(dims[1]) - 1
    time = sp.arange(ntime) *nh
    lat  = sp.linspace( matfile["minLat"][0,0], matfile["maxLat"][0,0], nlat )
    tlon = sp.linspace( matfile["minLon"][0,0], matfile["maxLon"][0,0], nlon+1 )
    lon  = tlon[:nlon]
    outfile.create_dimension('time', ntime)
    outfile.create_dimension('lat' , nlat)
    outfile.create_dimension('lon' , nlon)  
    #---------------------------------------------------
    # TIME
    if verbose: print "  Writing file coordinate: time ("+str(ntime)+")"
    time        = time.astype(np.int32, copy=False)     
    ftime       = outfile.create_variable('time', 'i', ('time',))
    ftime[:]    = time[:ntime]
    ftime.units = 'hours since '+yr+'-'+mn+'-01 00:00:00'
    del time
    #---------------------------------------------------
    # LATITUDE
    if verbose: print "  Writing file coordinate: lat ("+str(nlat)+")"
    flat        = outfile.create_variable('lat', 'd', ('lat',))
    flat[:]     = lat
    flat.units  = 'degrees_north'
    #---------------------------------------------------
    #LONGITUDE
    if verbose: print "  Writing file coordinate: lon ("+str(nlon)+")"
    flon        = outfile.create_variable('lon', 'd', ('lon',))
    flon[:]     = lon
    flon.units  = 'degrees_east'
    #---------------------------------------------------
    #---------------------------------------------------
    for var in var_name:
        if var=="mosaicTPW"         : var_info = ["mm","Total Precipitable Water"]
        if var=="mosaicTPWbefore"   : var_info = ["mm","Total Precipitable Water"]
        if var=="mosaicTPWafter"    : var_info = ["mm","Total Precipitable Water"]
        if var=="tdBeforeMx"        : var_info = ["hr","td After Mx"]
        if var=="tdAfterMx"         : var_info = ["hr","td Before Mx"]
        if var=="TPW_TEND"          : var_info = ["mm/hr","TPW Tendency"]
        ftmp = outfile.create_variable(var, 'd', ('time','lat','lon'))
        ftmp.units          = var_info[0]
        ftmp.long_name      = var_info[1]
        ftmp._FillValue     = mval
        ftmp.missing_value  = mval
        
        dlast = days_per_month[m-1]
        if debug : dlast = 1
        for d in range(1,dlast+1):
            for h in range(0,24):
                #if m==7 and d==5 and h==22: continue               # not sure what's wrong with this file!
                first = (h==0 and d==0 and var==var_name[0])
                dy = "{:0>2d}".format(d)
                hr = "{:0>2d}".format(h)
                ifile = idir+yr+mn+dy+"T"+hr+"0000vars.mat"
                #ifile = "/data2/whannah/OBS/MIMIC/raw_mat/20140705T220000vars.mat"
                if h%nh==0: print "    "+ifile
                matfile = spio.loadmat(ifile, verify_compressed_data_integrity=False)
                #---------------------------------------------------
                # print verbose information if verbose==True
                #---------------------------------------------------
                if verbose and first:
                    print
                    print matfile['__header__']
                    print matfile['__globals__']
                    print matfile['__version__']
                    print("Input matlab file variables:")
                    for key in matfile.keys():
                        if type(matfile[key]).__name__ == 'ndarray':
                           print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__,"  ",matfile[key].shape
                        else:
                           print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__
                    print
                #---------------------------------------------------
                # Load data from mat files
                #---------------------------------------------------
                if first and verbose: print "  Writing file data: "+var+" ",tmp.shape
                if var=="TPW_TEND":
                    iTPW1 = matfile['mosaicTPWbefore'][:,:nlon] #.astype(np.float32, copy=False)
                    iTPW2 = matfile['mosaicTPWafter'] [:,:nlon] #.astype(np.float32, copy=False)
                    itd1  = matfile['tdBeforeMx']     [:,:nlon] #.astype(np.float32, copy=False)
                    itd2  = matfile['tdAfterMx']      [:,:nlon] #.astype(np.float32, copy=False)
                    itd1  = np.where( ~np.isfinite( itd1), mval,  itd1 )
                    itd2  = np.where( ~np.isfinite( itd2), mval,  itd2 )
                    iTPW1 = np.where( ~np.isfinite(iTPW1), mval, iTPW1 )
                    iTPW2 = np.where( ~np.isfinite(iTPW2), mval, iTPW2 )
                    
                    iTPW1 = np.where( iTPW1>1e3, mval, iTPW1 )
                    iTPW2 = np.where( iTPW2>1e3, mval, iTPW2 )
                    iTPW1 = np.where( iTPW1==0, mval, iTPW1 )
                    iTPW2 = np.where( iTPW2==0, mval, iTPW2 )
                    
                    #itd1  = ma.masked_where(  itd1>1e3,  itd1 )
                    #itd2  = ma.masked_where(  itd2>1e3,  itd2 )
                    #iTPW1 = ma.masked_where( iTPW1>1e3, iTPW1 )
                    #iTPW2 = ma.masked_where( iTPW2>1e3, iTPW2 )
                    #iTPW1 = ma.masked_where( iTPW1==0,  iTPW1 )
                    #iTPW2 = ma.masked_where( iTPW2==0,  iTPW2 )
                    #iTPW1 = ma.masked_invalid( iTPW1 )
                    #iTPW2 = ma.masked_invalid( iTPW2 )
                    #itd1.fill_value  = mval
                    #itd2.fill_value  = mval
                    #iTPW1.fill_value = mval
                    #iTPW2.fill_value = mval
                    
                    if False :
                        tmp = iTPW2-iTPW1
                        #tmp = itd2
                        print tmp.min()
                        print tmp.mean()
                        print tmp.max()
                        print
                        exit()
                        
                    itmp = ( iTPW2 - iTPW1 ) / ( itd2 + itd1 )
                    
                    itmp = np.where( iTPW1==mval, mval, itmp )
                    itmp = np.where( iTPW2==mval, mval, itmp )
                    itmp = np.where( itd1 ==mval, mval, itmp )
                    itmp = np.where( itd1 ==mval, mval, itmp )
                    
                    if False :
                        tmp = itmp
                        print tmp.min()
                        #print tmp.mean()
                        print tmp.max()
                        print
                        #exit()
                    
                    del iTPW1,iTPW2,itd1,itd2
                else: 
                    itmp = matfile[var][:,:nlon].astype(np.float32, copy=False)
                #---------------------------------------------------
                # Handle missing values (NaN's and large numbers)
                #---------------------------------------------------
                #itmp = np.where( np.isnan(itmp), mval, itmp )
                itmp = np.where( ~np.isfinite(itmp), mval, itmp ) 
                tmp = ma.masked_where( itmp>1e10, itmp )
                tmp.fill_value = mval
                
                if var=="mosaicTPW" : 
                    mask = np.where( tmp>=65.5, 1, 0 )
                    tmp = np.where( mask==1, mval, tmp )
                    
                #print np.average(itmp)
                #exit()
                
                #---------------------------------------------------
                # write data to NetCDF file
                #---------------------------------------------------
                if h%nh==0 : otmp = tmp
                
                # old method - forget why I switched
                #if h%nh!=0 : 
                #    c1 = float(h%nh)-1.
                #    c2 = float(h%nh)
                #    otmp = ( tmp +  c1*otmp  )/c2 
                #if h%nh==(nh-1) : 
                #    ftmp[(d-1)*nt+h/nh,:,:] = otmp
                #    del itmp,tmp,otmp
                
                ftmp[(d-1)*nt+h/nh,:,:] = tmp
                
    #---------------------------------------------------
    # Close the NetCDF file
    #---------------------------------------------------
    outfile.close()
    print
    print ""+ofile
    print
    if debug: exit()
    
#=========================================================================================
#=========================================================================================
