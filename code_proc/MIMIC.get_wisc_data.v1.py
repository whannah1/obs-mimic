#!/usr/bin/env python
#=========================================================================================
# 	This script converts MIMIC data from the tropic.ssec.wisc.edu server (i.e. SRC_DIR)
#	to a local directory specified by DST_DIR
#	Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#
#	Oct, 2014	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy as np
import scipy.io
#=========================================================================================
#=========================================================================================
server 		= ""
#cmd 		= "rsync -uavt "
cmd 		= "wget"
file_type 	= "area"

SRC_DIR 	= "http://tropic.ssec.wisc.edu/real-time/mimic-tpw/global/areas/"
              
DST_DIR 	= "~/Data/OBS/MIMIC/raw_wisc_area/"

yr = "2008"
m1 = 1
m2 = 12
#=========================================================================================
#=========================================================================================
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

for m in range(m1,m2+1):
	mn = "{:0>2d}".format(m)
	for d in range(1,days_per_month[m-1]+1):
		dy = "{:0>2d}".format(d)
		for h in range(0,24):
			hr = "{:0>2d}".format(h)
			file_name = yr+mn+dy+"T"+hr+"0000."+file_type			
			SUB_DIR = yr+"/"+mn+"/"
			print SUB_DIR
			os.system(cmd+" "+SRC_DIR+SUB_DIR+file_name+" -P "+DST_DIR)
#=========================================================================================
#=========================================================================================
