#!/usr/bin/env python
#=========================================================================================
# 	This script converts all available MIMIC hourly *.mat files from idir 
#	v1 -> into daily NetCDF files that contain 24 hourly samples
#	v2 -> into monthly NetCDF files that contain 6 hourly samples
#	output files are written to odir
#	The motivation is to have an up-to-date directory of NetCDF MIMIC data
#	So, this does not overwrite NetCDF files which are already present
#
#	SciPy.io has a 'loadmat' function that is used for reading '.mat' files
#
#	The PyNIO (Nio) package is used for writing NetCDF files
#	and can be obtained from https://www.earthsystemgrid.org/home.htm
#
#    July, 2013	Walter Hannah 		University of Miami
#=========================================================================================
import os
import datetime
import numpy as np
import scipy as sp
import numpy.ma as ma
import scipy.io as spio
import Nio
#=========================================================================================
#=========================================================================================
debug    = True 	# writes to 'testfile' instead of to odir
verbose	 = False	# verbose output for debugging

testfile = '/home/whannah/Research/OBS/MIMIC/test.nc'	# only for debug mode
	
#idir = "/Users/whannah/Research/OBS/MIMIC/raw_mat/"		# input directory
#odir = "/Volumes/Amaterasu/obs/MIMIC/data/"				# output directory
idir = "/data2/whannah/OBS/MIMIC/raw_mat/"			# input directory
odir = "/data2/whannah/OBS/MIMIC/data/"				# output directory

#define variables that will go into output NetCDF file
#var_name = ["mosaicTPW","mosaicTPWbefore","mosaicTPWafter","tdBeforeMx","tdAfterMx"]
var_name = ["mosaicTPW","TPW_TEND"]

now = datetime.datetime.now()
yr = now.year
mn = now.month
#=========================================================================================
#=========================================================================================
mval = 9.969209968386869e+36
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]
print

ntime = 24

ifiles = os.listdir(idir)
nfiles = len(ifiles)

if debug : nfiles = 2

for ifile in ifiles[:nfiles]:
	print "	ifile: "+ifile
	yr = ifile[:4]
	mn = ifile[4:6]
	dy = ifile[6:8]
	#---------------------------------------------------
	# Check for output NetCDF file
	#---------------------------------------------------
	ofile = odir+"MIMIC.v2."+yr+mn+dy+".nc"
	if debug: ofile  = testfile
	#if os.path.isfile(ofile): os.remove(ofile)
	#outfile 		= Nio.open_file(ofile, "c")
	first = False
	if not os.path.isfile(ofile): first = True
	if     os.path.isfile(ofile): outfile = Nio.open_file(ofile, "w")
	if not os.path.isfile(ofile): outfile = Nio.open_file(ofile, "c")
	outfile.history = ""
	print "	ofile: "+ofile
	#---------------------------------------------------
	# Create coordinate variables and write to NetCDF
	#---------------------------------------------------
	matfile = spio.loadmat(idir+ifile)
	dims = matfile["mosaicTPW"].shape
	nlat = int(dims[0])
	nlon = int(dims[1]) - 1
	if first:	
		time = sp.arange(ntime)
		lat  = sp.linspace( matfile["minLat"][0,0], matfile["maxLat"][0,0], nlat )
		tlon = sp.linspace( matfile["minLon"][0,0], matfile["maxLon"][0,0], nlon+1 )
		lon  = tlon[:nlon]
		outfile.create_dimension('time', ntime)
		outfile.create_dimension('lat' , nlat)
		outfile.create_dimension('lon' , nlon)	
		#---------------------------------------------------
		# TIME
		if verbose: print "  Writing file coordinate: time ("+str(ntime)+")"
		time 		= time.astype(np.int32, copy=False)		
		ftime 		= outfile.create_variable('time', 'i', ('time',))
		ftime[:]	= time[:ntime]
		ftime.units = 'hours since '+yr+'-'+mn+'-01 00:00:00'
		del time
		#---------------------------------------------------
		# LATITUDE
		if verbose: print "  Writing file coordinate: lat ("+str(nlat)+")"
		flat 		= outfile.create_variable('lat', 'd', ('lat',))
		flat[:]	 	= lat
		flat.units 	= 'degrees_north'
		#---------------------------------------------------
		#LONGITUDE
		if verbose: print "  Writing file coordinate: lon ("+str(nlon)+")"
		flon 		= outfile.create_variable('lon', 'd', ('lon',))
		flon[:]	 	= lon
		flon.units 	= 'degrees_east'
	#---------------------------------------------------
	# Loop through variables
	#---------------------------------------------------
	for var in var_name:
		if var=="mosaicTPW" 		: var_info = ["mm","Total Precipitable Water"]
		if var=="mosaicTPWbefore" 	: var_info = ["mm","Total Precipitable Water"]
		if var=="mosaicTPWafter" 	: var_info = ["mm","Total Precipitable Water"]
		if var=="tdBeforeMx" 		: var_info = ["s","td After Mx"]
		if var=="tdAfterMx" 		: var_info = ["s","td Before Mx"]
		if var=="TPW_TEND"	 		: var_info = ["mm/s","TPW Tendency"]
		if first :
			ftmp = outfile.create_variable(var, 'd', ('time','lat','lon'))
			ftmp.units 			= var_info[0]
			ftmp.long_name		= var_info[1]
			ftmp._FillValue 	= mval
			ftmp.missing_value 	= mval
		if not first :
			ftmp = outfile.variables[var]
		
		for h in range(0,ntime):
			hr = "{:0>2d}".format(h)
			ifile = idir+yr+mn+dy+"T"+hr+"0000vars.mat"
			matfile = spio.loadmat(ifile)
			#---------------------------------------------------
			# print verbose information if verbose==True
			#---------------------------------------------------
			if verbose and h==0:
				print
				print matfile['__header__']
				print matfile['__globals__']
				print matfile['__version__']
				print("Input matlab file variables:")
				for key in matfile.keys():
					if type(matfile[key]).__name__ == 'ndarray':
					   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__,"  ",matfile[key].shape
					else:
					   print "    "+key.ljust(15)+"  ",type(matfile[key]).__name__
				print
			#---------------------------------------------------
			# Load data from mat files
			#---------------------------------------------------
			if verbose: print "  Writing file data: "+var#+" ",tmp.shape
			if var=="TPW_TEND":
				iTPW1 = matfile['mosaicTPWbefore'][:,:nlon]#.astype(np.float32, copy=False)
				iTPW2 = matfile['mosaicTPWafter'][:,:nlon]#.astype(np.float32, copy=False)
				itd1  = matfile['tdBeforeMx'][:,:nlon]#.astype(np.float32, copy=False)
				itd2  = matfile['tdAfterMx'][:,:nlon]#.astype(np.float32, copy=False)
				itmp = ( iTPW2 - iTPW1 ) / ( itd2 + itd1 )
				del iTPW1,iTPW2,itd1,itd2
			else: 
				itmp = matfile[var][:,:nlon].astype(np.float32, copy=False)
			#---------------------------------------------------
			# Handle missing values (NaN's and large numbers)
			#---------------------------------------------------
			#itmp = np.where( np.isnan(itmp), mval, itmp )
			itmp = np.where( ~np.isfinite(itmp), mval, itmp )
			tmp = ma.masked_where( itmp>1e10, itmp )
			tmp.fill_value = mval
			
			#---------------------------------------------------
			# write data to NetCDF file
			#---------------------------------------------------
			ftmp[h,:,:] = tmp
			#---------------------------------------------------
			# Code for making 6-hour averages
			#---------------------------------------------------
			#if h%6==0 : otmp = tmp
			#if h%6!=0 : 
			#	c1 = float(h%6)-1.
			#	c2 = float(h%6)
			#	otmp = ( tmp +  c1*otmp  )/c2 
			#if h%6==5 : 
			#	ftmp[(d-1)*4+h/6,:,:] = otmp
			#	del itmp,tmp,otmp
			#---------------------------------------------------
			#---------------------------------------------------
	#---------------------------------------------------
	# Close the NetCDF file
	#---------------------------------------------------
	outfile.close()
	print
	#if debug: exit()
#=========================================================================================
#=========================================================================================
