load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/calendar_decode2.ncl"
begin

    ifile = "/data2/whannah/OBS/ERAi+MIMIC.6hour.budget.q.2014.nc"
    ;ifile = "~/Research/OBS/ERAi+MIMIC.6hour.budget.q.2014.alt.nc"

	fig_type = "png"
	
;=====================================================================================
;=====================================================================================
    infile = addfile(ifile,"r")
    
	time = infile->time_3h

	TPW = infile->MIMIC_TPW
	AT  = infile->MIMIC_AT

    U = infile->U(:,{700},:,:)
    V = infile->V(:,{700},:,:)
    
    U = where(ismissing(TPW(::2,:,:)),U@_FillValue,U)
    V = where(ismissing(TPW(::2,:,:)),V@_FillValue,V)

    U@units = "m/s"
    V@units = "m/s"
    
    U@long_name = "U wind"
    V@long_name = "V wind"
;====================================================================================================
;====================================================================================================
ctime = calendar_decode2(time,-5)
do t = 0,dimsizes(time)
	
	
	
	yr = ctime(t,0)
	mn = sprinti("%2.2i",ctime(t,1))
	dy = sprinti("%2.2i",ctime(t,2))
	hr = sprinti("%2.2i",ctime(t,3))
	
	stime = yr+"-"+mn+"-"+dy+"  "+hr+"Z "
	;stime = ctime(t,3)+"Z "+ctime(t,1)+"-"+ctime(t,2)+"-"+ctime(t,0)
	fig_file = "~/Research/OBS/MIMIC/images/MIMIC.AT."+yr+"-"+mn+"-"+dy+"."+hr+""

	;fig_file = "~/Research/OBS/MIMIC/images/MIMIC.AT."+sprinti("%3.3i",t)
	
	fig_type@wkWidth	= 2048
	fig_type@wkHeight	= 2048
	wks = gsn_open_wks(fig_type,fig_file)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmYLMinorOn                     = False
		res@tmXBMinorOn                     = False
		res@gsnAddCyclic                    = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@lbTitleFontHeightF              = 0.012
		res@lbLabelFontHeightF              = 0.012
		res@tmXBLabelFontHeightF            = 0.008
		res@tmYLLabelFontHeightF            = 0.008
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@pmLabelBarOrthogonalPosF        = 0.12
		res@gsnLeftString                   = ""
		res@gsnCenterString					= ""
		res@gsnRightString                  = ""
		vres = res
		res@cnLineLabelsOn                  = False
		res@cnInfoLabelOn                   = False
		res@gsnLeftString              		= ""
		res@cnLevelSelectionMode			= "ExplicitLevels"		
	;----------------------------------------------------------------
	; Plot TPW analysis tendency
	;----------------------------------------------------------------	
		tres = res
		tres@mpLimitMode		= "LatLon"
		tres@mpMinLatF			= -30
		tres@mpMaxLatF			=  30
		tres@mpMinLonF			=  30
		tres@mpMaxLonF			= 175
		tres@gsnCenterString	= stime
	
		
		;res@cnFillPallette	= "WhiteBlueGreenYellowRed"
		;tres@cnFillPalette	= "MPL_RdYlGn"
		tres@cnFillPalette	= "BlueRed"
		if isatt(tres,"cnLevels") then delete(tres@cnLevels) end if	
		tres@cnFillOn		= True
		tres@cnLinesOn		= False
		tres@cnFillMode		= "RasterFill"
		tres@cnLevels		= ispan(-50,50,2)
		
	plot = gsn_csm_contour_map(wks,AT(t,:,:),tres)
	;----------------------------------------------------------------
	; Add TPW contours
	;----------------------------------------------------------------
		if isatt(res,"cnLevels") then delete(res@cnLevels) end if
		res@cnFillOn			= False
		res@cnLinesOn			= True
		res@cnLevels			= ispan(25,65,10)
		res@cnLineColor			= "black"
		res@cnLineThicknessF	= 3. 
	
	overlay(plot,gsn_csm_contour(wks,TPW(t,:,:),res))
	;----------------------------------------------------------------
	; Add thick line to make TPW plateau
	;----------------------------------------------------------------
		if isatt(res,"cnLevels") then delete(res@cnLevels) end if
		res@cnFillOn			= False
		res@cnLinesOn			= True
		res@cnLevels			= ispan(50,50,1)
		res@cnLineThicknessF	= 6. 
		
	overlay(plot,gsn_csm_contour(wks,TPW(t,:,:),res))
	;----------------------------------------------------------------
	; Add vectors
	;----------------------------------------------------------------
		vres@gsnDraw            	= False
		vres@gsnFrame           	= False
		vres@vcRefMagnitudeF 		= 5.0                ; define vector ref mag
		vres@vcRefLengthF    		= 0.02               ; define length of vec ref
		vres@vcGlyphStyle    		= "CurlyVector"      ; turn on curley vectors
		vres@vcMinDistanceF  		= 0.02               ; thin out vectors
		vres@vcLineArrowThicknessF	= 2.
		vres@vcLineArrowColor		= "blue"
	
	ut = t/2
	overlay(plot,gsn_csm_vector(wks,U(t,:,:),V(t,:,:),vres))
	;----------------------------------------------------------------
	;----------------------------------------------------------------	
	draw(plot)
	frame(wks)

    print("    "+fig_file+"."+fig_type)
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
    
	delete([/res/])
end do
;====================================================================================================
;====================================================================================================
end
